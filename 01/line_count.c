#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int main(int argc, char *argv[]) {
    if (argc < 2)
        return 0;
    
    int fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    int line_count = 0;
    char buf;
    while (read(fd, &buf, 1) > 0)
        if (buf == '\n')
            line_count++;

    close(fd);

    printf("%d lines\n", line_count);
    return 0;
}
