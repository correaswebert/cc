LEX
---

Lex is a program that generates lexical analyzers. It is
generally used with the YACC parser generator. It was
developed by Mike Lesk and Eric Schmidt at Bell Labs using
the C programming language. It is the standard lexical
analyzer on many Unix systems.

Lexical analyzer is a program that transforms an input
stream into a sequence of tokens. It translates a set of
regular expression specifications (given as input in
input_file.l) into a C implementation of a corresponding
finite state machine (lex.yy.c). This C program, when
compiled, yields an executable lexical analyzer. 

Lex turns the user's expression and actions into the host
general-purpose language, called the yylex program. This
program recognizes expressions in the input stream and
performs the specified actions.


YACC
----

YACC is an acronym for **Yet Another Compiler-Compiler**. It was developed by Stephen Johnson at Bell Labs. It is a Look Ahead
Left-to-Right (LALR) parser generator. It generates a LALR parser,
which tries to make sense of the syntax of the source code. This
LALR parser is based on formal grammar.

YACC translates a given context free grammar (CFG) into its corresponding
push-down automata in the C programming language, This program when
compiled gives an executable parser called yyparse. The output stream of
yylex from lex is used as an input, which finally returns the parsed output.


File Structure
--------------

Both the programs follow a similar structure. They are split into three parts, delimited by **%%**.

```
%{

DEFINITIONS

%}

%%

RULES

%%

AUXILIARY FUNCTIONS
```
> Definitions
* Lex: declarations of constants and macros of C.
* YACC: information about the tokens, their associativity, and declarations of C macros and constants.

> Rules
* Lex: regexes with corresponding C code actions
* YACC: CFGs with corresponding C code actions

> Auxilary Functions
* additional procedures that might be required, like `yyerror`, `yywrap` etc.


WORKING
-------

* YACC compiles the grammar rules in the `.y` file, and generates output file called `y.tab.c` and the relevant header file called `y.tab.h` using the `-d` flag
* This `y.tab.h` is included in the `.l` lex code, allowing the usage of tokens defined by the YACC file, and generates `lex.yy.c`.
* Then both the output files of yacc and lex are compiled and linked using `gcc`, generating the final executable file `a.out`.


Example
-------

```
lex bas.l

yacc -d bas.y

gcc lex.yy.c y.tab.c -o bas.exe

./bas.exe
```

![Design](./lex_yacc.png)

