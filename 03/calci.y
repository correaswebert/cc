%{
#include <stdio.h>
#include <stdlib.h>

int exit_code = EXIT_SUCCESS;

int yylex();
void yyerror();
%}

%token NUMBER

%left '+' '-'
%left '*' '/'
%left '%'
%left '(' ')'

%%

P : E { printf("%d\n", $$); }
;

E : E '+' E     { $$ = $1 + $3; }
  | E '-' E     { $$ = $1 - $3; }
  | E '*' E     { $$ = $1 * $3; }
  | E '/' E     { $$ = $1 - $3; }
  | E '%' E     { $$ = $1 - $3; }
  | '('E')'     { $$ = $2; }
  | NUMBER      { $$ = $1; }
;

%%

int main() {
    printf("> ");
    yyparse();
    return exit_code;
}

void yyerror() {
    exit_code = EXIT_FAILURE;
    printf("Invalid expression\n");
}

int yywrap() {
    return 1;
}
