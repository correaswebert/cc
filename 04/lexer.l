/*Lex code to count total number of tokens */

%{

int n = 0 ;

%}

%%

"int"|"float"|"while"|"for"|"if"|"else"|"struct" {
    printf("keyword:    %s\n", yytext);
} 

[a-zA-Z_][a-zA-Z0-9_]* {
    printf("identifier: %s\n", yytext);
}

"<="|"=="|"="|"++"|"--"|"-"|"*"|"+"|"%" {
    printf("operator:   %s\n", yytext);
}

[(){}|,;] {
    printf("separator:  %s\n", yytext);
}

[0-9]*"."[0-9]+ {
    printf("float:      %s\n", yytext);
}

[0-9]+ {
    printf("integer:    %s\n", yytext);
}						

. ;

%%

