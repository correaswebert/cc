
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

bool char_in_array(char c, char arr[])
{
    for (int i = 0; arr[i] != 0; i++)
        if (c == arr[i])
            return true;
    return false;
}

bool str_in_array(char *s, char *arr[])
{
    for (int i = 0; arr[i] != NULL; i++)
        if (!strcmp(s, arr[i]))
            return true;
    return false;
}

bool is_delimiter(char c)
{
    char delimiters[] = {' ',
                         '+',
                         '-',
                         '*',
                         '/',
                         ',',
                         ';',
                         '>',
                         '<',
                         '=',
                         '(',
                         ')',
                         '[',
                         ']',
                         '{',
                         '}',
                         0};

    return char_in_array(c, delimiters);
}

bool is_operator(char c)
{
    char operators[] = {
        '+',
        '-',
        '*',
        '/',
        '>',
        '<',
        '=',
        0};

    return char_in_array(c, operators);
}

bool valid_identifier(char *str)
{
    if ((str[0] >= '0' && str[0] <= '9') || is_delimiter(str[0]))
        return false;
    return true;
}

bool is_keyword(char *str)
{
    char *keywords[] = {
        "if",
        "else",
        "while",
        "do",
        "break",
        "continue",
        "int",
        "double",
        "float",
        "return",
        "char",
        "case",
        "char",
        "sizeof",
        "long",
        "short",
        "typedef",
        "switch",
        "unsigned",
        "void",
        "static",
        "struct",
        "goto",
        NULL};

    return str_in_array(str, keywords);
}

bool is_integer(char *str)
{
    int len = strlen(str);

    if (len == 0)
        return false;

    for (int i = 0; i < len; i++)
        if (!(str[i] >= '0' && str[i] <= '9'))
            return false;
    return true;
}

bool is_float(char *str)
{
    int len = strlen(str);
    bool hasDecimal = false;

    if (len == 0)
        return false;

    for (int i = 0; i < len; i++)
    {
        if (!(str[i] >= '0' && str[i] <= '9') && str[i] != '.' ||
            (str[i] == '-' && i > 0))
            return (false);
        if (str[i] == '.')
            hasDecimal = true;
    }
    return (hasDecimal);
}

char *substring(char *str, int left, int right)
{
    int i;
    char *substr = (char *)malloc(sizeof(char) * (right - left + 2));

    for (i = left; i <= right; i++)
        substr[i - left] = str[i];
    substr[right - left + 1] = '\0';
    return (substr);
}

void parse(char *str)
{
    int left = 0, right = 0;
    int len = strlen(str);

    while (right <= len && left <= right)
    {
        if (!is_delimiter(str[right]))
            right++;

        if (is_delimiter(str[right]) && left == right)
        {
            if (is_operator(str[right]) == true)
                printf("operator:   %c\n", str[right]);
            else
            {
                if (str[right] != ' ')
                    printf("separator:  %c\n", str[right]);
            }

            right++;
            left = right;
        }

        else if (is_delimiter(str[right]) && left != right || (right == len && left != right))
        {
            char *substr = substring(str, left, right - 1);

            if (is_keyword(substr))
                printf("keyword:    %s\n", substr);

            else if (is_integer(substr))
                printf("integer:    %s\n", substr);

            else if (is_float(substr))
                printf("float:      %s\n", substr);

            else if (valid_identifier(substr) && !is_delimiter(str[right - 1]))
                printf("identifier: %s\n", substr);

            else if (!valid_identifier(substr) && !is_delimiter(str[right - 1]))
                printf("'%s' IS NOT A VALID IDENTIFIER\n", substr);
            left = right;
        }
    }
    return;
}

int main(int argc, char *argv[])
{
    const int MAX_LIMIT = 1024;
    char str[MAX_LIMIT];

    while (true)
    {
        // fgets(str, MAX_LIMIT, stdin);
        scanf("%[^\n]%*c", str);
        parse(str);
        printf("\n");
    }

    return EXIT_SUCCESS;
}
